#include "DecoderNetwork.h"

Decoder::Decoder()
{
    // defaults
    is_opened = 0;
    is_end = 0;
    seconds = 0;
    frame_count = 0;
    duration = 0;
    fps = 0;
    bitrate_video = 0;
    pixel_format = "";
    width = 0;
    height = 0;
    format_ctx = 0;

    codec_name_video = "";
    stream_video_id = -1;
    stream_video = 0;
    codec_ctx_video = 0;
    codec_video = 0;
    video_last_ts = 0;

    codec_name_audio = "";
    stream_audio_id = -1;
    stream_audio = 0;
    codec_ctx_audio = 0;
    codec_audio = 0;
    audio_last_ts = 0;

    sws_ctx = 0;
    out_rgb = 0;

    options = 0;

    address = "";
    port = DEFAULT_PORT;
    channel = DEFAULT_CHANNEL;
}

Decoder::~Decoder()
{
    release();
}

int Decoder::open()
{
    // release first
    if(is_opened) {
        release();
    }
    int ret = 0;

    // prepare input string
    std::string input = address + ":" + std::to_string(port);

    // set options
    av_dict_set(&options, "timeout", std::to_string((int) timeout * 1000).c_str(), 0);

    // open input allocate format context
    ret = avformat_open_input(&format_ctx, input.c_str(), NULL, &options);
    if (ret < 0) {
        release();
        return ret;
    }

    // retrieve stream information
    ret = avformat_find_stream_info(format_ctx, NULL);
    if (ret < 0) {
        release();
        return ret;
    }

    //
    // VIDEO
    //

    // set video stream id
    ret = av_find_best_stream(format_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (ret < 0) {
        release();
        return ret;
    }
    stream_video_id = ret;

    // set video stream
    stream_video = format_ctx->streams[stream_video_id];
    if (!stream_video) {
        release();
        return -1;
    }

    // find video decoder
    codec_video = avcodec_find_decoder(stream_video->codec->codec_id);
    if (!codec_video) {
        release();
        return -1;
    }

    // alloc video codec context
    codec_ctx_video = avcodec_alloc_context3(codec_video);
    avcodec_copy_context(codec_ctx_video, stream_video->codec);

    // open video codec
    ret = avcodec_open2(codec_ctx_video, codec_video, NULL);
    if (ret < 0) {
        release();
        return ret;
    }
    codec_name_video = avcodec_get_name(codec_ctx_video->codec_id);

    // allocate video frame
    width = codec_ctx_video->width;
    height = codec_ctx_video->height;
    //    frame_video = av_frame_alloc();
    //    frame_video->width = width;
    //    frame_video->height = height;
    //    frame_video->format = codec_ctx_video->pix_fmt;
    //    ret = av_image_alloc(frame_video->data, frame_video->linesize,
    //                         frame_video->width, frame_video->height,
    //                         (AVPixelFormat) frame_video->format, 32);
    //    if (ret < 0) {
    //        release();
    //        return ret;
    //    }

    // get useful info
    frame_count = stream_video->nb_frames;
    duration = stream_video->duration;
    double den = stream_video->avg_frame_rate.den;
    if(den > 0) fps = stream_video->avg_frame_rate.num / den;

    bitrate_video = codec_ctx_video->bit_rate;
    pixel_format = av_get_pix_fmt_name((AVPixelFormat) codec_ctx_video->pix_fmt);
    seconds = 0;

    //
    // AUDIO
    //

    // set audio stream id
    ret = av_find_best_stream(format_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
    if (ret < 0) {
        release();
        return ret;
    }
    stream_audio_id = ret;

    // set audio stream
    stream_audio = format_ctx->streams[stream_audio_id];
    if (!stream_audio) {
        release();
        return -1;
    }

    // find audio decoder
    codec_audio = avcodec_find_decoder(stream_audio->codec->codec_id);
    if (!codec_audio) {
        release();
        return -1;
    }

    // alloc audio codec context
    codec_ctx_audio = avcodec_alloc_context3(codec_audio);
    avcodec_copy_context(codec_ctx_audio, stream_audio->codec);

    // open audio codec
    ret = avcodec_open2(codec_ctx_audio, codec_audio, NULL);
    if (ret < 0) {
        release();
        return ret;
    }
    codec_name_audio = avcodec_get_name(codec_ctx_audio->codec_id);

    // get useful info
    bitrate_audio = codec_ctx_audio->bit_rate;
    samplerate_audio = codec_ctx_audio->sample_rate;

    //
    // OPTIONS
    //

    // channel
    if(channel > 0) {
        std::string map = "0:p:" + std::to_string(channel);
        av_opt_set(codec_ctx_video, "map", map.c_str(), AV_OPT_SEARCH_CHILDREN);
        av_opt_set(codec_ctx_audio, "map", map.c_str(), AV_OPT_SEARCH_CHILDREN);
    }

    // udp, delay and packet buffer size
    // cout << av_opt_set_int(format_ctx, "analyzeduration", 100000000, AV_OPT_SEARCH_CHILDREN);
    av_opt_set(format_ctx, "rtsp_transport", "udp", AV_OPT_SEARCH_CHILDREN);
    av_opt_set_int(format_ctx, "max_delay", 5000000, AV_OPT_SEARCH_CHILDREN);
    av_opt_set_int(format_ctx, "buffer_size", 1600000, AV_OPT_SEARCH_CHILDREN);

    // number of threads
    // av_opt_set_int(codec_ctx_video, "threads", 0, AV_OPT_SEARCH_CHILDREN);

    // set opened
    is_opened = true;
    is_end = false;

    //
    // DEBUGGING
    //

    // rgb conversion for video frame check
    if(out_rgb)
    {
        frame_video_out = av_frame_alloc();
        if(!frame_video_out) {
            release();
            return -1;
        }
        frame_video_out->width = width;
        frame_video_out->height = height;
        frame_video_out->format = AV_PIX_FMT_RGB24;
        ret = av_image_alloc(frame_video_out->data, frame_video_out->linesize,
                             frame_video_out->width, frame_video_out->height,
                             (AVPixelFormat) frame_video_out->format, 32);
        if (ret < 0) {
            release();
            return -1;
        }
        if(!sws_ctx) {
            sws_ctx = sws_getContext(width, height, (AVPixelFormat) codec_ctx_video->pix_fmt,
                                     width, height, (AVPixelFormat) frame_video_out->format,
                                     SWS_FAST_BILINEAR,
                                     NULL, NULL, NULL);
            if(!sws_ctx) {
                release();
                return -1;
            }
        }
    }

    // return success
    return 0;
}

void Decoder::start()
{
    // not opened return
    if(!is_opened) return;

    // clear stale frames
    std::queue<AVFrame *>().swap(frames_video);
    std::queue<AVFrame *>().swap(frames_audio);

    // run capture thread
    thread = new std::thread( [&] {
        const uint packet_size = codec_ctx_audio->sample_rate / fps;
        std::vector<uint8_t> audio_data;

        while(true)
        {
            // stop signal
            if(is_end) return;

            // packet init
            pkt.data = NULL;
            pkt.size = 0;
            av_init_packet(&pkt);
            int ret = 0;

            // read next packet
            ret = av_read_frame(format_ctx, &pkt);

            // bad packet
            if(ret < 0 || NULL == pkt.data) {
                av_free_packet(&pkt);
                continue;
            }

            // audio packet
            if(pkt.stream_index == stream_audio_id) {
                // save last timestamp
                if(pkt.pts != AV_NOPTS_VALUE) {
                    audio_last_ts = pkt.pts / (double) MPEGTS_FREQ;
                }

                // allocate audio frame
                AVFrame * frame_audio = av_frame_alloc();

                // decode audio packet
                int gotFrame;
                ret = avcodec_decode_audio4(codec_ctx_audio, frame_audio, &gotFrame, &pkt);
                frame_audio->pts = pkt.pts;
                av_free_packet(&pkt);

                // add to middle layer buffer
                for(int i = 0; i < frame_audio->channels; ++i) {
                    std::vector<uint8_t> samples(frame_audio->data[i], frame_audio->data[i] + frame_audio->linesize[0]);
                    audio_data.insert(std::end(audio_data), std::begin(samples), std::end(samples));
                }

                // add to main buffer if enough samples are received
                int ps_c = packet_size * frame_audio->channels * (frame_audio->linesize[0] / frame_audio->nb_samples);
                if(audio_data.size() >= ps_c) {
                    std::vector<uint8_t> * packet = new std::vector<uint8_t>(
                                audio_data.begin(),
                                audio_data.begin() + ps_c);


                    // prepare audio frame
                    AVFrame * frame_audio_big = av_frame_alloc();
                    av_frame_copy_props(frame_audio_big, frame_audio);
                    frame_audio_big->format = (AVSampleFormat) frame_audio->format;
                    frame_audio_big->nb_samples = packet_size;
                    av_frame_set_pkt_size(frame_audio_big, packet_size);
                    av_frame_set_channels(frame_audio_big, frame_audio->channels);
                    av_frame_set_channel_layout(frame_audio_big, frame_audio->channel_layout);
                    av_frame_get_buffer(frame_audio_big, 32);
                    ret = av_samples_fill_arrays(frame_audio_big->data, frame_audio_big->linesize,
                                                 packet->data(), frame_audio->channels, packet_size,
                                                 (AVSampleFormat) frame_audio->format, 0);

                    // !! TODO think about the best tstamp mechanism
                    av_frame_set_best_effort_timestamp(frame_audio_big, (video_last_ts - 1. / fps) * MPEGTS_FREQ);
                    frame_audio_big->pts = (video_last_ts - 1. / fps) * MPEGTS_FREQ;

                    // av_frame_set_best_effort_timestamp(frame_audio_big, audio_next_ts);
                    // frame_audio_big->pts = audio_next_ts;

                    // remove old data
                    audio_data.erase(audio_data.begin(), audio_data.begin() + ps_c);

                    pushAudioFrame(frame_audio_big);
                }
                av_frame_free(&frame_audio);

                continue;
            }
            // video packet
            else if(pkt.stream_index == stream_video_id) {
                // save last timestamp
                if(pkt.pts != AV_NOPTS_VALUE) {
                    video_last_ts = pkt.pts / (double) MPEGTS_FREQ;
                }

                // decode video packet
                AVFrame * frame_video = av_frame_alloc();
                int gotFrame;
                ret = avcodec_decode_video2(codec_ctx_video, frame_video, &gotFrame, &pkt);
                frame_video->pts = pkt.pts;

                av_free_packet(&pkt);

                if(out_rgb) {
                    // convert to rgb
                    sws_scale(sws_ctx,
                              frame_video->data, frame_video->linesize,
                              0, frame_video->height,
                              frame_video_out->data, frame_video_out->linesize);
                }

                // add to buffer
                //std::cout << "pushing V" << std::endl;
                pushVideoFrame(frame_video);

                continue;
            }
            else {
                continue;
            }
        }
    });



}

void Decoder::stop()
{
    // interrupt thread with signal
    is_end = true;

    // wait for thread exits
    thread->join();
    delete thread;
}

AVFrame * Decoder::getAudioFrame()
{
    // wait until buffer has an element
    std::unique_lock<std::mutex> lock(ma);
    ca.wait(lock, [&] { return frames_audio.size(); });

    // clone first and remove
    AVFrame * f_a = frames_audio.front();
    AVFrame * frame = av_frame_clone(f_a);
    av_frame_free(&f_a);
    frames_audio.pop();

    lock.unlock();

    return frame;
}

AVFrame * Decoder::getVideoFrame()
{
    // wait until buffer has an element
    std::unique_lock<std::mutex> lock(mv);
    cv.wait(lock, [&] { return frames_video.size(); });

    // clone first and remove
    AVFrame * f_v = frames_video.front();
    AVFrame * frame = av_frame_clone(f_v);
    av_frame_free(&f_v);
    frames_video.pop();

    lock.unlock();

    return frame;
}

void Decoder::release()
{
    if(!is_opened)
        return;

    // memory cleanup
    std::queue<AVFrame *>().swap(frames_video);
    std::queue<AVFrame *>().swap(frames_audio);

    if (!(format_ctx->flags & AVFMT_NOFILE))
        avformat_close_input(&format_ctx);
    avcodec_free_context(&codec_ctx_video);
    avcodec_free_context(&codec_ctx_audio);
    avformat_free_context(format_ctx);

    // defaults
    is_end = 0;
    is_opened = 0;
    seconds = 0;
    frame_count = 0;
    duration = 0;
    fps = 0;
    bitrate_video = 0;
    pixel_format = "";
    width = 0;
    height = 0;
    format_ctx = 0;

    codec_name_video = "";
    stream_video_id = -1;
    stream_video = 0;
    codec_ctx_video = 0;
    codec_video = 0;

    codec_name_audio = "";
    stream_audio_id = -1;
    stream_audio = 0;
    codec_ctx_audio = 0;
    codec_audio = 0;

    sws_ctx = 0;
    out_rgb = 0;

    options = 0;

    address = "";
    port = DEFAULT_PORT;
    channel = DEFAULT_CHANNEL;
}

void Decoder::pushAudioFrame(AVFrame * frame)
{
    ma.lock();

    // push and deallocate
    frames_audio.push(frame);
    if(frames_audio.size() > MAX_BUFFER_SIZE) {
        AVFrame * f_a = frames_audio.front();
        av_frame_free(&f_a);
        frames_audio.pop();
    }

    ma.unlock();

    // wake up consumer thread
    ca.notify_one();
}

void Decoder::pushVideoFrame(AVFrame * frame)
{
    mv.lock();

    // push and deallocate
    frames_video.push(frame);
    if(frames_video.size() > MAX_BUFFER_SIZE) {
        AVFrame * f_v = frames_video.front();
        av_frame_free(&f_v);
        frames_video.pop();
    }

    mv.unlock();

    // wake up consumer thread
    cv.notify_one();
}
