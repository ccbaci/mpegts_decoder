#ifndef DECODERNETWORK_H
#define DECODERNETWORK_H

// avcodec, avformat, swscale, avutil
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
}

// definitions
#define DEFAULT_PORT 1234
#define DEFAULT_CHANNEL -1
#define MPEGTS_FREQ 90000. // 90000 kHz
#define MAX_BUFFER_SIZE 2

// STL
#include <iostream>
#include <string>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

class Decoder
{
public:
    Decoder();
    ~Decoder();

    //
    // PARAMS
    // to set before open
    //

    // param address
    // please also provide the protocol with the host address
    // udp://hostname
    // tcp://hostname
    // http://hostname etc
    std::string address;

    // param port
    // please provide
    // default is 1234
    int port;

    // param channel
    // please provide program number
    // default -1 means select first program
    int channel;

    // only for debugging
    // convert to rgb for display purposes
    bool out_rgb;

    // timeout for blocking calls (e.g. av_read_frame) in miliseconds
    int timeout;

    //
    // METHODS
    //

    // opens and starts buffering the network input address:port|channel
    // returns 0 on success, error code on failure
    int open();

    // starts capture and filling A and V buffers within a new thread
    void start();

    // stops capture
    void stop();

    // get audio or video frame
    // !!! BLOCKS until buffers have an element
    AVFrame * getAudioFrame();
    AVFrame * getVideoFrame();

    // releases the decoder
    void release();

    //
    // INTERNAL
    //

    // opened or not
    bool is_opened;

    // current seconds while stream is started
    double seconds;

    // read video frame count and duration
    // = 0 for live streams
    // > 0 for static streams
    int frame_count;
    double duration;

    // read fps
    double fps;

    // get input format. mpegts, mp4, rawvideo etc.
    AVFormatContext * format_ctx;

    // for rgb conversion if out_rgb is set for debugging purposes
    SwsContext * sws_ctx;

    // push to buffers
    // deallocates and removes stale frames if MAX_BUFFER_SIZE is reached
    void pushAudioFrame(AVFrame *);
    void pushVideoFrame(AVFrame *);

    //
    // VIDEO
    //

    // video stream
    AVStream * stream_video;
    int stream_video_id;

    // video codec
    AVCodecContext * codec_ctx_video;
    AVCodec * codec_video;
    std::string codec_name_video;
    double bitrate_video;

    // video frame
    // AVFrame * frame_video;
    AVFrame * frame_video_out;
    int width;
    int height;
    std::string pixel_format;

    // video frames
    std::queue<AVFrame *> frames_video;

    //
    // AUDIO
    //

    // audio stream
    AVStream * stream_audio;
    int stream_audio_id;

    // audio codec
    AVCodecContext * codec_ctx_audio;
    AVCodec * codec_audio;
    std::string codec_name_audio;
    double bitrate_audio;
    double samplerate_audio;

    // audio frames
    std::queue<AVFrame *> samples_audio;
    std::queue<AVFrame *> frames_audio;

    //
    // COMMON
    //

    // options
    AVDictionary * options;

    // packet and last saved tstamps in seconds
    AVPacket pkt;
    double audio_last_ts, video_last_ts;

    // thread, mutex and waiting conditions for access to buffers
    std::thread * thread;
    std::condition_variable ca, cv;
    std::mutex ma, mv;
    bool is_end;

    // dump info
    friend std::ostream & operator<<(std::ostream &os, const Decoder & decoder) {
        std::string input = decoder.address + ":" + std::to_string(decoder.port);
        os << input << std::endl;
        os << "fps : " << decoder.fps << std::endl;
        os << "duration : " << decoder.duration << std::endl;
        os << "video bitrate : " << decoder.bitrate_video << std::endl;
        os << "video codec : " << decoder.codec_name_video << std::endl;
        os << "width : " << decoder.width << std::endl;
        os << "height : " << decoder.height << std::endl;
        os << "pixel format : " << decoder.pixel_format << std::endl;

        os << "audio bitrate : " << decoder.bitrate_audio << std::endl;
        os << "audio samplerate : " << decoder.samplerate_audio << std::endl;
        os << "audio codec : " << decoder.codec_name_audio << std::endl;
        os << "-------------" << std::endl;
        return os;
    }
};


#endif // DECODERNETWORK_H
