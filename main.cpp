#include "DecoderNetwork.h"

int main(int argc, char *argv[])
{
    // register all codecs and muxers
    // only errors will be written to console
    av_register_all();
    avcodec_register_all();
    avformat_network_init();
    av_log_set_level(AV_LOG_ERROR);

    // set decoder options
    Decoder decoder;
    decoder.address = "udp://127.0.0.1";
    decoder.port = 2222;
    decoder.channel = -1;
    decoder.timeout = 20000;
    decoder.out_rgb = 0;

    // open capture
    // blocks until network stream is up
    decoder.open();

    // show info
    std::cout << decoder;

    // start capture
    decoder.start();

    int n = 0;
    while(n < 10000) {
        // get frames
        // blocks until audio buffer has a frame
        AVFrame * f_a = decoder.getAudioFrame();

        // blocks until video buffer has a frame
        AVFrame * f_v = decoder.getVideoFrame();

        // bad frame
        if(NULL == f_a || NULL == f_v) {
            std::cout << "NULL" << std::endl;
            std::cout << "-----------" << std::endl;
            continue;
        }

        // show info
        std::cout << "audio ts (in seconds) " << av_frame_get_best_effort_timestamp(f_a) / MPEGTS_FREQ << std::endl;
        std::cout << "nb channels " << f_a->channels << std::endl;
        std::cout << "nb samples " << f_a->nb_samples << std::endl;
        std::cout << "linesize " << f_a->linesize[0] << std::endl;
        std::cout << "format " << av_get_sample_fmt_name((AVSampleFormat) f_a->format) << std::endl;
        std::cout << "sample rate " << f_a->sample_rate << std::endl;
        std::cout << "pkt size " << f_a->pkt_size << std::endl;

        std::cout << "video ts (in seconds) " << av_frame_get_best_effort_timestamp(f_v) / MPEGTS_FREQ << std::endl;
        std::cout << "width " << f_v->width << std::endl;
        std::cout << "height " << f_v->height << std::endl;
        std::cout << "pixel format " << av_get_pix_fmt_name((AVPixelFormat) f_v->format) << std::endl;
        std::cout << "-----------" << std::endl;

        av_frame_free(&f_a);
        av_frame_free(&f_v);
        n++;
    }

    // stop capture
    decoder.stop();

    return 0;
}
